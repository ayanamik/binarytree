﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BinaryTreeApplication.Models
{
    public class Node
    {
        public int? value { get; set; }
        public Node left { get; set; }
        public Node right { get; set; }

        public Node(int? data)
        {
            value = data;
            right = null;
            left = null;
        }

    }
}