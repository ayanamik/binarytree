﻿using BinaryTreeApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BinaryTreeApplication.Controllers
{
    public class NodesController : Controller
    {        
        private Node root; 
        List<int?> values = new List<int?>();
        
        public ActionResult BinaryTree()
        { 
            return View();  
        }

        [HttpPost]
        public ActionResult BinaryTree(string action)  
        {
            int value;               
            int nodeSearch1;
            int nodeSearch2;
            Node parent;

            switch (action)
            {
                case "Insertar":

                    var pru = Request.Form["data"];
                    if (pru == "")
                    {
                        ViewBag.Value = "Debe ingresar un valor";
                        break;
                    }                        

                    value = Convert.ToInt32(Request.Form["data"]);                    

                    if (Session["data"] != null)
                        values = (List<int?>)Session["data"];
                    
                    values.Add(value);
                                        
                    Session["data"] = values;

                    ViewBag.Value = value;
                    break;

                case "Buscar":
                    var pru1 = Request.Form["nodeSearch1"]; 
                    var pru2 = Request.Form["nodeSearch2"];
                    if (pru1 == "" || pru2 == "")
                    {
                        ViewBag.Parent = "Debe ingresar un valor";
                        break;
                    }

                    values = (List<int?>)Session["data"];

                    foreach (var n in values)
                    {
                        root = Insert(root, n);
                    }                    
                                       
                    nodeSearch1 = Convert.ToInt32(Request.Form["nodeSearch1"]);
                    nodeSearch2 = Convert.ToInt32(Request.Form["nodeSearch2"]);

                    parent = Ancestor(root, nodeSearch1, nodeSearch2);                     

                    if (parent != null) { ViewBag.Parent = parent.value; }                        
                    else { ViewBag.Parent = "El ancestro no existe"; }
                    break;
            }          

                                          
            return View("BinaryTree", root);            

        }

        public Node Insert(Node current, int? data)  
        { 
            if (current == null)
            {
                return new Node(data);           
            }

            if (data < current.value)
            {
                current.left = Insert(current.left, data);
            }
            else
            {
                current.right = Insert(current.right, data);
            }

            return current;
        }                

        public Node Ancestor(Node node, int n1, int n2)
        {
            if (node == null)
            {
                return null;  
            }
            
            if (node.value > n1 && node.value > n2)
            {
                return Ancestor(node.left, n1, n2);
            }
            
            if (node.value < n1 && node.value < n2)
            {
                return Ancestor(node.right, n1, n2);
            }

            return node;  
        }


    }
}