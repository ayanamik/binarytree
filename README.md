---------------
  Binary Tree
---------------

Prerequisitos:
Visual Studio 2015

Ejecución:
Abrir la solución desde visual studio 2015.
Para ejecutar la solución, Debug -> Start Debugging, ó con F5.

Interacción:
La aplicación consta de dos partes, en la primera se crea el árbol.
Para esto se debe insertar un valor numérico en la casilla y luego 
dar click en el botón "Insertar". Luego de insertar el valor, en la 
parte inferior se muestra el valor insertado. 

En la segunda parte se consulta el ancestro común entre dos nodos a partir 
del árbol creado con los valores anteriores. Se debe insertar en cada una 
de las casillas el valor del nodo a consultar, luego dar click en "Buscar". 
En la parte inferior el sistema muestra el ancestro encontrado.




